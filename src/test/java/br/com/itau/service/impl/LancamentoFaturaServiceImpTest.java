package br.com.itau.service.impl;

import br.com.itau.dto.Lancamento;
import br.com.itau.service.CategoriasService;
import br.com.itau.service.LancamentoFaturaService;
import br.com.itau.service.LancamentosService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
public class LancamentoFaturaServiceImpTest {

    @Mock
    private LancamentoFaturaService lancamentoFaturaService;

    @Mock
    private LancamentoFaturaServiceImp lancamentoFaturaServiceImp;

    @Mock
    private LancamentosService lancamentosService;

    @Mock
    private CategoriasService categoriasService;

    @Test
    public void shouldReturnAllLancamentos() {

        Lancamento data = new Lancamento();
        List<Lancamento> result = new ArrayList<>();

        data.setId(1L);
        data.setCategoria(1);
        data.setOrigem("Mercado");
        data.setValor(15.90);
        data.setMesLancamento(1);

        result.add(data);

        when(lancamentosService.getLancamentos()).thenReturn(result);

        Lancamento lancamentos = lancamentosService.getLancamentos().get(0);

        Assert.assertNotNull(result.get(0));

    }
}