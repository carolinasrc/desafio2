package br.com.itau.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Lancamento {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("valor")
    private Double valor;

    @JsonProperty("origem")
    private String origem;

    @JsonProperty("categoria")
    private Integer categoria;

    @JsonProperty("mes_lancamento")
    private Integer mesLancamento;

    public Lancamento() {

    }

    @Override
    public String toString() {
        return "Lancamento [id=" + id + ", valor=" + valor + ", origem=" + origem + ", categoria=" + categoria
                + ", mes=" + mesLancamento + "]";
    }

}
