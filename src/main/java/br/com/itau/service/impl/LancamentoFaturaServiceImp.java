package br.com.itau.service.impl;

import br.com.itau.dto.Categoria;
import br.com.itau.dto.Lancamento;
import br.com.itau.service.CategoriasService;
import br.com.itau.service.LancamentoFaturaService;
import br.com.itau.service.LancamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

@Service("LancamentoFaturaService")
public class LancamentoFaturaServiceImp implements LancamentoFaturaService {

    @Autowired
    private LancamentosService lancamentosService;

    @Autowired
    private CategoriasService categoriasService;

    @Override
    public List<Lancamento> getAllLancamentos() {

        /* Busca todos os lançamentos ordernando por mês */
        List<Lancamento> lancamentos = lancamentosService.getLancamentos()
                .stream()
                .sorted(comparing(Lancamento::getMesLancamento))
                .collect(Collectors.toList());

        return lancamentos;
    }

    @Override
    public List<Categoria> getAllCategorias() {

        /* Busca todas as categorias */
        List<Categoria> categorias = categoriasService.getCategorias();
        return categorias;
    }

    @Override
    public Optional<Categoria> getCategoriasById(Long id) {

        /* Busca uma categoria específica passando o id como parâmetro */
        Optional<Categoria> categoria = categoriasService.getCategorias()
                .stream()
                .filter(cat -> cat.getId() == id)
                .findFirst();

        return categoria;
    }

    @Override
    public Optional<Lancamento> getLancamentoById(Long id) {

        /* Busca um lançamento específico passando o id como parâmetro */
        Optional<Lancamento> lancamento = lancamentosService.getLancamentos()
                .stream()
                .filter(lanc -> lanc.getId() == id)
                .findFirst();

        return lancamento;
    }

    @Override
    public List<Lancamento> getLancamentosByCategoria(Integer id) {

        /* Busca um lançamento por categoria passando o id como parâmetro */
        List<Lancamento> lancamentos = lancamentosService.getLancamentos()
                .stream()
                .filter(lanc -> lanc.getCategoria() == id)
                .sorted(comparing(Lancamento::getMesLancamento))
                .collect(Collectors.toList());

        return lancamentos;
    }
}

