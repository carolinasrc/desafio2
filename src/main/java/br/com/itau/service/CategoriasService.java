package br.com.itau.service;

import br.com.itau.dto.Categoria;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(url= "https://desafio-web-itau.herokuapp.com/categorias" , name = "categorias")
public interface CategoriasService {

    @GetMapping()
    List<Categoria> getCategorias();
}
