package br.com.itau.service;

import br.com.itau.dto.Lancamento;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(url = "https://desafio-web-itau.herokuapp.com/lancamentos", name = "lancamentos")
public interface LancamentosService {

    @GetMapping()
    List<Lancamento> getLancamentos();
}
