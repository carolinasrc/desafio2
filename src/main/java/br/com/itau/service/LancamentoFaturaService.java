package br.com.itau.service;

import br.com.itau.dto.Categoria;
import br.com.itau.dto.Lancamento;

import java.util.List;
import java.util.Optional;

public interface LancamentoFaturaService {

    public List<Lancamento> getAllLancamentos();

    public List<Categoria> getAllCategorias();

    public Optional<Categoria> getCategoriasById(Long id);

    public Optional<Lancamento> getLancamentoById(Long id);

    public List<Lancamento> getLancamentosByCategoria(Integer id);

}
