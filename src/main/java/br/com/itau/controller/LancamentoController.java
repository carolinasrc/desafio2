package br.com.itau.controller;

import br.com.itau.dto.Categoria;
import br.com.itau.dto.Lancamento;
import br.com.itau.service.impl.LancamentoFaturaServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class LancamentoController {

    @Autowired
    private LancamentoFaturaServiceImp lancamentoFatura;

    @GetMapping("lancamentos")
    public ResponseEntity getLancamentos() {

        List<Lancamento> lancamentos = lancamentoFatura.getAllLancamentos();

        return lancamentos != null ? ResponseEntity.ok().body(lancamentos) : ResponseEntity.notFound().build();
    }

    @GetMapping("categorias")
    public ResponseEntity getCategorias() {

        List<Categoria> categorias = lancamentoFatura.getAllCategorias();
        return categorias != null ? ResponseEntity.ok().body(categorias) : ResponseEntity.notFound().build();
    }

    @GetMapping("categoria/{id}")
    public ResponseEntity getCategoriaById(@PathVariable Long id) {

        Optional<Categoria> categoria = lancamentoFatura.getCategoriasById(id);

        return categoria != null ? ResponseEntity.ok().body(categoria) : ResponseEntity.notFound().build();
    }

    @GetMapping("lancamento/{id}")
    public ResponseEntity getLancamentoById(@PathVariable Long id) {

        Optional<Lancamento> lancamento = lancamentoFatura.getLancamentoById(id);

        return lancamento != null ? ResponseEntity.ok().body(lancamento) : ResponseEntity.notFound().build();
    }

    @GetMapping("lancamento-categoria/{id}")
    public ResponseEntity getLancamentoByCategoria(@PathVariable Integer id) {

        List<Lancamento> lancamentos = lancamentoFatura.getLancamentosByCategoria(id);

        return lancamentos != null ? ResponseEntity.ok().body(lancamentos) : ResponseEntity.notFound().build();

    }
}
