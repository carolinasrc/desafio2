# Proposta Histórico Fatura

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!.

Esse é nosso desafio 2 de 3, onde iremos avaliar sua habilidade com o Spring Framework.  
Para isso você irá consumir uma API onde estão os lançamentos de um cartão de credito dentro de um mesmo ano. Instruções de consumo da API [**neste link**](https://gitlab.com/desafio-web/rest-api).

O método de avaliação é simples, tendo seu código em mãos iremos rodar o projeto e validar se você construiu o projeto atendendo ao cenário proposto e os requisitos.

Boa sorte!

### Cenário Proposto

_Neste projeto temos uma API que retorna os lançamentos de um cartão de forma desorganizada._  
_A necessidade do nosso projeto é criar uma API Rest que consuma a API citada acima e mostre os dados de forma organizada._

### Requisitos

* Deve ser criado endpoints para:
    *   Listar categorias.
    *   Listar lançamentos.
    *   Obter categoria por id.
    *   Obter Lançamento por id.
    *   Obter lançamento por categoria.
* Deve ser usado o Spring Framework.
* Arquitetura MVC
* Os retornos da sua API devem ser em formato JSON
* Criar testes unitários para todas as classes.
* Essa tarefa deve ser entregue em um repositório público no Gitlab, o link desse repositório deve ser enviado para Felipe Marques Melo felipe.melo@itau-unibanco.com.br com seu nome completo e o link do git.


### Dica

* Atente-se a organização do projeto isso será levado em conta.

**ATENÇÃO:** _Após o envio do repositório para o e-mail descrito nos requisitos não serão aceitos novos commits, essa atividade deve ser enviada até 20/04/2020 10:00._ 

### Bom Desempenho!

### #QueremosVoceNoItau

## PRÓXIMO PASSO: [DESAFIO 3 / 3](https://gitlab.com/desafio-web/desafio3)
